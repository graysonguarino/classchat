# ClassChat
This application incorporates a chat room with two elements, `client.py` and `server.py`. No non-native modules are utilized, and the program is intended for use with Python 3.9.
To run the client, simply run `python3 client.py`. To run the server, run `python3 server.py`. Currently, the server port is always set to `5005`, so be sure to enter that when asked for server port number in the client.
This version of ClassChat enters a chat room by default, but private messages can be sent by sending /userlist to the server to retrieve a list of usernames, then by typing `@[username] [message]` in the chat bar. For instance, if `/serverlist` returns `[‘self’, ‘bob’]`, I can say hi to user bob by sending `@bob hi!`

**Implementation**

**Client**

The client works by first asking the user for the IP address and port for the server then for their username. The TCP connection is created and the client utilizes `select()` to call `receive_message()` to check for new messages while also using `select()` to poll for keyboard input. This cuts down significantly on CPU usage. `receive_message()` reads the message in from the client socket, processes and formats it, then prints it out to the user. This function calls two others during processing, `strip_message_text()` which returns a string of the message text from the message JSON, and `check_if_private()`, which checks the JSON to see if there is a `destination_username`, the presence of which implies a private message and so (privately) is printed alongside the sender’s username before the message.
When a message is to be sent, the keyboard poller calls `send_message()` which first checks if the message starts with “@”, in which case the `destination_username` is changed from its initial value of None to a string with all following characters in the message before the next space. For example, if a message starts with @bob, bob is extracted from the message text and is placed in the `destination_username` field of the message JSON. The JSON is fully populated, encoded into UTF-8, and a header is created and encoded to describe the length of the JSON.

**Server**


When the server receives a message from a client, the initial header size is 10 bytes. The first thing to be received over TCP is the username header which tells how long the username is, then the TCP port is listened to for the number of bytes of that header. This same process is repeated with the message header and message itself. Similarly to the client, the socket reads which are conducted in `check_connections()` are controlled by `select()`. If an error is detected with a user, including a connection closed by client, `disconnect_user()` is called to remove the client socket from the `sockets_list` and the clients dictionary.
  
A feature has been added to get a list of connected users through the `send_user_list()` function. When a user sends the message `/userlist`, the server replies with a list of all usernames of connected users. When the client attempts to private message a user, the server checked upon message retrieval if the `destination_username` field is populated inside the message JSON, and if so, `get_client_index_from_username()` is called to find the index of the client in the clients dictionary. If the username matches a connected user, the server only forwards the message to that specific user.
More in-depth information about my implementation is written in the comments of both `client.py` and `server.py`.
