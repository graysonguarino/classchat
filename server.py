# server.py
# The server for the ClassChat application

import socket
import select
import json

HEADER_LENGTH = 10
HOSTNAME = socket.gethostname()
BUFFER_SIZE = 4096

TCP_IP = socket.gethostbyname(HOSTNAME)
TCP_PORT = 5005

# Create socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Setting socket options to reuse address
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# Bind socket to IP and port
server_socket.bind((TCP_IP, TCP_PORT))
# Listen to port
server_socket.listen()
# List of all sockets of client + server socket
sockets_list = [server_socket]
# Client connection info
clients = {}


# int get_client_index_from_username(<str> username)
# Takes in a string of a username and returns the index of it in the clients{} dict, or None if not found.
def get_client_index_from_username(username):
    client_values_list = clients.values()
    for index, user in enumerate(client_values_list):
        if username == user['data'].decode('utf-8'):
            return index
    else:
        return None


# JSON create_json(<int> status, <str> destination_username, <str> text)
# Takes in a status (always 1 currently), a destination_username, and the message text and returns a JSON object.
def create_json(status, destination_username, text):
    message_dict = {
        "status": str(status),
        "destination_username": destination_username,
        "text": text
    }
    message_json = json.dumps(message_dict)
    return message_json


# str[] make_user_list()
# Returns a list of the usernames of currently connected users.
def make_user_list():
    user_list = []
    clients_list = clients.values()
    for user in clients_list:
        user_list.append(user['data'].decode('utf-8'))
    return user_list


# void send_user_list(<socket> client_socket)
# Called when a client sends "/userlist". Creates a JSON with the user_list generated in make_user_list() then sends
# it to the client.
def send_user_list(client_socket):
    user_list = str(make_user_list())
    message = create_json(1, None, user_list).encode('utf-8')
    message_header = f"{len(message):<{HEADER_LENGTH}}".encode('utf-8')
    server_name = "server".encode('utf-8')
    server_header = f"{len(server_name):<{HEADER_LENGTH}}".encode('utf-8')
    client_socket.send(server_header + server_name + message_header + message)


# void forward_message(<int> client_index, <socket> notified_socket, <str> message)
# Called when a message is received from a client and needs to be forwarded either through the group chat or private
# message.
def forward_message(client_index, notified_socket, message):
    user = clients[notified_socket]
    if client_index is None:
        for client_socket in clients:  # Send to ALL clients (except sender)
            if client_socket != notified_socket:  # Don't repeat message to sender
                client_socket.send(user['header'] + user['data'] + message['header'] + message['data'])
    else:
        client_list = list(clients)
        client_list[client_index].send(user['header'] + user['data'] + message['header'] + message['data'])


# void disconnect_user(<socket> notified_socket)
# Removes a user from sockets_list and clients.
def disconnect_user(notified_socket):
    print(f"User {clients[notified_socket]['data'].decode('utf-8')} closed connection")
    sockets_list.remove(notified_socket)  # Remove disconnected client socket from sockets_list
    del clients[notified_socket]


# dict receive_message(<socket> client_socket)
# Retrieves the message from the client socket and returns its header and message JSON in a dict.
def receive_message(client_socket):
    try:
        message_header = client_socket.recv(HEADER_LENGTH)
        if not len(message_header):  # if we did not receive data
            return False

        message_length = int(message_header.decode("utf-8").strip())
        message = client_socket.recv(message_length)
        # Return dict with message header and message data
        return {"header": message_header, "data": message}
    except Exception as e:
        print(e)
        return False


# void get_message(<socket> notified_socket)
# Calls receive_message to get the raw_message JSON from the socket, then
def get_message(notified_socket):
    raw_message = receive_message(notified_socket)

    if raw_message is False:
        disconnect_user(notified_socket)
        return

    message_json = raw_message['data'].decode('utf-8')
    message = json.loads(message_json)
    user = clients[notified_socket]

    if message['text'] == "/userlist\n":
        send_user_list(notified_socket)  # may not work
        print("Sending userlist")
        return

    destination_username = None
    if message['destination_username'] is not None:
        destination_username = message['destination_username']

    print(f"Received message from {user['data'].decode('utf-8')}: {message['text']}")

    if destination_username is not None:
        client_index = get_client_index_from_username(destination_username)
        if client_index is None:
            pass
        else:
            forward_message(client_index, notified_socket, raw_message)
    else:
        forward_message(None, notified_socket, raw_message)


# void check_connections(<select> read_sockets, <select> exception_sockets
# Checks for users that are newly connecting or disconnecting.
def check_connections(read_sockets, exception_sockets):
    for notified_socket in read_sockets:
        if notified_socket == server_socket:  # Accept client connection
            client_socket, client_address = server_socket.accept()
            user = receive_message(client_socket)

            # Upon disconnect
            if user is False:
                continue

            sockets_list.append(client_socket)
            clients[client_socket] = user
            print(f"User {user['data'].decode('utf-8')} has connected from {client_address[0]}:{client_address[1]}")
        else:
            get_message(notified_socket)

    for notified_socket in exception_sockets:  # Remove faulty socket
        disconnect_user(notified_socket)


while True:
    # Socket read list, write list, error list
    read_sockets, _, exception_sockets = select.select(sockets_list, [], sockets_list)
    check_connections(read_sockets, exception_sockets)

