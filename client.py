# client.py
# The client for the ClassChat application

import socket
import select
import errno
import sys
import json
import threading

HEADER_LENGTH = 10
HOSTNAME = socket.gethostname()
BUFFER_SIZE = 1024

TCP_IP = input("Enter server IP [format xxx.xxx.xxx.xxx]: ")
TCP_PORT = int(input("Enter server port number: "))


input_username = input("Please enter your username: ")
# Create a socket for communication
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Configure TCP protocol with IP address of server and port number
# Connect with server through socket
client_socket.connect((TCP_IP, TCP_PORT))
client_socket.setblocking(False)
print("\nSuccessfully connected. Send a message by typing then hitting ENTER. "
      "For list of connected users, send /userlist. \nTo send a private message, type @username before your message.")

username = input_username.encode('utf-8')
username_header = f"{len(username):<{HEADER_LENGTH}}".encode('utf-8')
client_socket.send(username_header + username)


# JSON create_json(<int> status, <str> destination_username, <str> text)
# Takes in a status (always 1 currently), a destination_username, and the message text and returns a JSON object.
def create_json(status, destination_username, text):
    message_dict = {
        "status": str(status),
        "destination_username": destination_username,
        "text": text
    }
    message_json = json.dumps(message_dict)
    return message_json


# str strip_message_text(<JSON> message)
# Returns the message text as a strong when given a message JSON
def strip_message_text(message):
    return json.loads(message)['text']


# bool check_if_private(<JSON> message)
# Checks if message is as private message by seeing if destination_username is None. Returns True if private, else False
def check_if_private(message):
    destination_username = json.loads(message)['destination_username']
    if destination_username is not None:
        return True
    else:
        return False


# void send_message(<str> message_text)
# Takes in the string of a message, creates the message JSON, then sends it over client_socket.
def send_message(message_text):
    destination_username = None
    if message_text[:1] == "@":
        destination_username = message_text[message_text.find("@") + 1:].split()[0]
    message_json = create_json(1, destination_username, message_text)
    message_text = message_json.encode('utf-8')
    message_header = f"{len(message_text):<{HEADER_LENGTH}}".encode('utf-8')
    client_socket.send(message_header + message_text)


# void receive_message(<select> read_socket)
# Reads in the message from the client socket and prints it to the user.
def receive_message(read_sockets):
    try:  # Receive messages
        for read_socket in read_sockets:
            username_header = read_socket.recv(HEADER_LENGTH)
            if not len(username_header):  # Did not receive data
                print("Connection closed by server")
                sys.exit()
            username_len = int(username_header.decode('utf-8').strip())
            username = read_socket.recv(username_len).decode('utf-8')

            message_header = read_socket.recv(HEADER_LENGTH)
            message_len = int(message_header.decode('utf-8').strip())
            message = read_socket.recv(message_len).decode('utf-8')

            message_text = strip_message_text(message)
            is_private = check_if_private(message)

            if is_private:
                print(f"{username} (privately) > {message_text}")
            else:
                print(f"{username} > {message_text}")

    except IOError as e:  # No more messages to be received
        if e.errno != errno.EAGAIN or e.errno != errno.EWOULDBLOCK:
            print("Reading error: ", str(e))
            sys.exit()
    except IndexError as e:
        print("INDEX ERROR")
    except Exception as e:
        print("Exception: ", str(e))
        sys.exit()


while True:
    read_sockets, _, _ = select.select([client_socket], [], [], 0.001)
    receive_message(read_sockets)

    i, o, e = select.select([sys.stdin], [], [], 0.0001)
    while True:
        for s in i:
            if s == sys.stdin:
                message = sys.stdin.readline()
                if message:  # If message not blank, create then send message_json
                    send_message(message)
        break
